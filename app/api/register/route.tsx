import { NextRequest, NextResponse } from "next/server";
import z from 'zod'
import prisma from "@/prisma/client"
import bcrypt from 'bcrypt'

const schema = z.object({
    email: z.string().email(),
    password: z.string().min(5),
    confirmPassword: z.string().min(5)
})
.refine(data => data.password === data.confirmPassword, {
    message: "Password don't match",
    path: ["confirmaPassword"]
})

export const POST = async (request: NextRequest) => {
    const body = await request.json();
    const validation = schema.safeParse(body)
    if(!validation.success)
    return NextResponse.json(validation.error.errors, { status: 400});

    const user = await prisma.user.findUnique({
        where: {email: body.email}
    })

    if(user) return NextResponse.json({error: "User already exists"}, {status: 400});

    const hashedPassword = await bcrypt.hash(body.password, 10);

    const newUser = await prisma.user.create({
        data:{
            email: body.email,
            hashPassword: hashedPassword
        }
    })
    return NextResponse.json({"value": "'User created'"}, {status: 201})
}