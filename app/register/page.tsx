"use client"
import { useRouter } from 'next/navigation';
import React from 'react'
import {useForm} from 'react-hook-form';

export interface Props {
        email: string;
        password: string;
        confirmPassword: string;
}


const Register = () => {
    const {register, handleSubmit} = useForm();
    const router = useRouter();
    const onSubmit = async (data: any) => {
        await fetch('http://localhost:3000/api/register', {
            method: 'POST',
            body: JSON.stringify(data)
        }).then(res => {
            if(res.ok)
                router.push("/api/auth/signin")
        })
        .catch((error: Error) => alert(error.message))
    }


  return (
    <div className='h-screen flex items-center justify-center'>
        <form className='bg-white shadow-md rounded px-8 pt-6 pb-8 mb-4' onSubmit={handleSubmit(onSubmit)}>
            <div className='mb-6'>
                <input {...register('email')} type='email' id='email' className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="Email" required />
            </div>
            <div className='mb-6'>
                <input {...register('password')} type='password' id='password' className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5  dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="Password" required />
            </div>
            <div className='mb-6'>
                <input {...register('confirmPassword')} type='password' id='confirmPassword' className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5  dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="Confirm Password " required />
            </div>
            <div className='mb-6'>
                <button type="submit" className="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm w-full sm:w-auto px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">S'inscrire</button>      
            </div>
        </form>
    </div>
  )
}

export default Register